public class HelloWorld {

    public static void numSquared(int x) //the value declared in the () is what will be used when the method is called
    {
        int result = x * x;
        System.out.println(x + " squared equals " + result);
    }

    public static void numMultiply(float a, float b)
    {
        float c = a * b;
        System.out.println(a + " multiplied by " + b + " equals " + c);
    }

    public static String getFullName(String firstName, String lastName)
    {
        String fullname = firstName + " " + lastName;
        return fullname;
    }

    public static void main(String[] args) {

        String output = "Hello World!";
        System.out.println(output);

        String name = "Celia";
        String nameOutput = "My name is " + name + "!";
        System.out.println(nameOutput);

        String good = "lovely";
        String bad = "terrible";
        System.out.println("I hope you're having a " + good + " day!");

        ifStatementCode();
        System.out.println("Program completed successfully");

    }

    public static void ifStatementCode()
    {
        int x = 104;

        if (x < 13)
        {
            System.out.println("x is less than 10");
        }
        else if (x <= 103)
        {
            System.out.println("x is between 13 and 103");
        }
        else
        {
            System.out.println("x is greater than 103");
        }

        String[] animals = {"cat", "cat", "dog", "tortoise", "cat", "rabbit", "dog", "cat", "dog", "cat"};

        int numDogs = 0;
        int numCats = 0;

        for (int i = 0; i < animals.length; i++)
        {
            if (animals[i] == "cat")
            {
                numCats++;
            }
            else if (animals[i] == "dog")
            {
                numDogs++;
            }

            System.out.println("Number of cats: " + numCats);
            System.out.println("Number of dogs: " + numDogs);
        }

        int[] ages = {24, 31, 29, 40, 18, 20, 42, 50};

        int num = ages[0];

        for (int i=1; i < ages.length; i++)
        {
            if (num > ages[i]) {
                num = ages[i];
            }

            System.out.println("The smallest age is age is: " + num);
        }

        numSquared(6);
        numMultiply(4.5f, 4f);

        String firstName = "Celia";
        String lastName = "Perry";

        String fullname = getFullName(firstName, lastName);
        System.out.println(fullname);
    }
}
