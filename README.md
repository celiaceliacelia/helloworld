# HelloWorld

## Welcome!
This is Celia's HelloWorld application for CSC8427. 

## What's this project for?
This project is storing the submissions for my first few weeks of CSC8427, a module in my MSc Digital and Technology Solutions (Software Engineering).

## Contributors
Seeing as this is for my Masters Degree, I'm not really looking for any contributors other than myself at the moment.
## Useful Links

- [ ] Here are my [GitLab](https://gitlab.com/celiaceliacelia) and [GitHub](https://github.com/celiaceliacelia) accounts
- [ ] Here is my [LinkedIn](https://www.linkedin.com/in/celiaperry/) 

## Funny little box
```
I made a funny little box!
```